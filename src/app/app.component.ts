import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  listaPersonas:any = []
  cabeceras = ["Nombre",  "Edad", "Sexo", "Documento"];
  personaForm: FormGroup;
  archivo: File;
  documentoNombre:string;
  submit: boolean = false;
  personaID:Number;

  constructor(private http:HttpClient,private fb: FormBuilder,) {
    this.documentoNombre = '';
    this.crearFormulario();

   }

  ngOnInit(): void {
    this.loadPersonas();
  }

  loadPersonas(){
    this.http.get('https://randomapi.com/api/64f1597958d417af5cf0aea92bf99cf9').subscribe((response:any) => {
      this.listaPersonas = response.results[0]

    }
    )
  }

  crearFormulario(): void {
    this.personaForm = this.fb.group({
      documento: ['',Validators.required],
      nombre: ['', Validators.required],

    });
  }

  onSelectionChange(event: any) {
    this.personaID = event.target.value;
  }
  get f(): any {
    return this.personaForm.controls;
  }

  uploadFile(event: any): void {
    let elem = event.target;

    if (elem.files.length > 0) {
      this.archivo = event.target.files[0];
      this.documentoNombre = this.archivo.name;

    } 
  }

  borrarFormulario(): void {
    this.personaForm.reset();
    this.archivo = null;
    this.documentoNombre = '';
    this.submit = false;
  }

  onSubmit(): void {
    this.submit = true;

    if (this.personaForm.invalid) {
      alert('Complete los datos requeridos');
      window.scrollTo(0, 0);
    } else {
      this.guardarDocumento(this.documentoNombre,this.personaID)


      
    }
  }

  guardarDocumento(archivo,persona) {
    
      let values = localStorage.getItem('persona');
      let personas = values ? JSON.parse(values) : [];
      console.log(personas)
      let finalArr = personas.filter(function(d){
        if(d.id == persona){ return d;}
        });
        console.log(finalArr)
      if(finalArr.length == 0){
        personas.push({id: persona, documento: archivo});
        localStorage.setItem('persona', JSON.stringify(personas));      
      }else{
        let objIndex = personas.findIndex((obj => obj.id == persona));
        personas[objIndex].documento = archivo
        localStorage.setItem('persona', JSON.stringify(personas));      


      }
      
      this.borrarFormulario()

 }

 getDocumento(id){
  let arregloPersonas = JSON.parse(localStorage.getItem('persona'))
  if(arregloPersonas){
    let doc = arregloPersonas.find((x) => x.id == id )
    return doc
  }
 }

 getName(fullPath){
  return fullPath.replace(/^.*[\\\/]/, '');
}
}
